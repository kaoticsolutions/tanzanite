<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class GPlus {

    private static $gURL = null;

    public static function init() {
        
    }

    public static function hooks() {
        Hook::add("privmsg", array(__CLASS__, "getG"), array("command" => "/^!g$/"));
        Hook::add("gStart", array(__CLASS__, "setG"));
        Hook::add("gStop", array(__CLASS__, "closeG"));
    }

    public static function getG($nick, $ident, $host, $destination, $message, $command, $params) {
        if (!empty(self::$gURL)) {
            Bot::MSG($destination, "G+ " . self::$gURL);
        } else {
            Bot::MSG($destination, "No G+ running.");
        }
    }

    public static function setG($url) {
        if (self::$gURL != $url) {
            self::$gURL = $url;

            Bot::MSG("#kaos", "G+ Opened " . self::$gURL);
        }
    }

    public static function closeG() {
        self::$gURL = null;

        Bot::MSG("#kaos", "G+ Closed");
    }

}