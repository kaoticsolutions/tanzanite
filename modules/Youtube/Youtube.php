<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class YouTube {

    public static $regex = "/((https?:\/\/)?(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/v\/)([\w-]{11}).*|https?:\/\/(?:youtu\.be\/|(?:[a-z]{2,3}\.)?youtube\.com\/watch(?:\?|#\!)v=)([\w-]{11}).*)/i";
    private static $config;

    public static $APIs = array(
      'videos.list' => 'https://www.googleapis.com/youtube/v3/videos'
    );
    public $page_info = array();

    private function __construct($params = array())
    {
        $this->setApiKey($params['key']);
        if (array_key_exists('referer', $params)) {
            $this->setReferer($params['referer']);
        }
        if (array_key_exists('apis', $params)) {
            $this->setAPIs($params['apis']);
        }
    }

    public static function init() {
        self::$config = json_decode(file_get_contents(MODPATH . DIRECTORY_SEPARATOR . "Youtube" . DIRECTORY_SEPARATOR . "mod.json"));
    }

    public static function hooks() {
        Hook::add("privmsg", array(__CLASS__, "youtube"), array("message" => self::$regex));
    }

    private function setAPIs(array $APIs) {
        $this->APIs = $APIs;
    }

    private function setReferer($referer) {
        $this->referer = $referer;
    }



    public static function youtube($nick, $ident, $host, $destination, $message, $command, $params) {
        $id = self::linkifyYouTubeURLs($message);

        $youtube = self::getYoutube($id);

        $title = $youtube->snippet->title;
        $likes = $youtube->statistics->likeCount;
        $dislikes = $youtube->statistics->dislikeCount;
        $views = $youtube->statistics->viewCount;
        $author = $youtube->snippet->channelTitle;
        $uploaded = date("m/d/y @ H:i", strtotime($youtube->snippet->publishedAt));


        Bot::MSG($destination, htmlspecialchars_decode("[1,0You0,4Tube] $title [+$likes] [-$dislikes] [$views\\views] [$author - $uploaded]", ENT_COMPAT | ENT_HTML401 | ENT_QUOTES));
    }

    public function getYoutube($id) {
        $apiURL = self::getApi('videos.list');

        foreach (self::$config->youtube as $apiConfig) {
            $apiKey = $apiConfig->apikey;
        }

        $apiParams = array(
            'key' => $apiKey,
            'id' => $id,
            'part' => 'id, snippet, contentDetails, player, statistics, status'
        );
        $apiData = self::api_get($apiURL, $apiParams);
        return self::decodeSingle($apiData);
    }

    public function api_get($url, $params) {
        $tuCurl = curl_init();
        curl_setopt($tuCurl, CURLOPT_URL, $url . (strpos($url, '?') === false ? '?' : '') . http_build_query($params));
        curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, 1);
        $tuData = curl_exec($tuCurl);
        return $tuData;
    }

    public function getApi($name) {
        return self::$APIs[$name];
    }

    public function decodeSingle(&$apiData) {
        $resObj = json_decode($apiData);
        if (isset($resObj->error)) {
            $msg = "Error " . $resObj->error->code . " " . $resObj->error->message;
            if (isset($resObj->error->errors[0])) {
                $msg .= " : " . $resObj->error->errors[0]->reason;
            }
            throw new \Exception($msg, $resObj->error->code);
        } else {
            $itemsArray = $resObj->items;
            if (!is_array($itemsArray) || count($itemsArray) == 0) {
                return false;
            } else {
                return $itemsArray[0];
            }
        }
    }

    private static function linkifyYouTubeURLs($text) {
        $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20111012)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w]*      # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w-]*        # Consume any URL (query) remainder.
        ~ix', '$1', $text);
        return $text;
    }

}