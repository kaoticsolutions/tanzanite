<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class Linkcatcher {

    public static function init() {
        
    }

    public static function hooks() {
        Hook::add("privmsg", array(__CLASS__, "url"), array("message" => "/(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?/"));
    }

    public static function url($nick, $ident, $host, $destination, $message, $command, $params) {
        if (preg_match("/((https?:\/\/)([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?)/ ", $message, $matches)) {
            if (!preg_match(YouTube::$regex, $message)) {
                $str = file_get_contents($matches[1]);
                if (strlen($str) > 0) {
                    preg_match("/\<title\>(.*)\<\/title\>/", $str, $title);
                    Bot::MSG($destination, htmlspecialchars_decode($title[1], ENT_COMPAT | ENT_HTML401 | ENT_QUOTES));
                }
            }
        }
    }

}