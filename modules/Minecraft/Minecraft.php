<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class Minecraft {

    private static $gURL = null;
    private static $regexJoin = "#\[INFO\] (.*?)\[/(.*?):(.*?)\] logged in with entity id#";
    private static $regexQuit = "#\[INFO\] (.*?) lost connection: #";

    public static function init() {
        
    }

    public static function hooks() {
        Hook::add("MC", array(__CLASS__, "join"), array("line" => self::$regexJoin));
        Hook::add("MC", array(__CLASS__, "quit"), array("line" => self::$regexQuit));
    }

    public static function join($line) {
        if (preg_match(self::$regexJoin, $line, $matches)) {
        	Bot::MSG("#kaos", "{$matches[1]} joined Minecraft.");
        }
    }
    
    public static function quit($line) {
    	if (preg_match(self::$regexQuit, $line, $matches)) {
    		Bot::MSG("#kaos", "{$matches[1]} ragequit Minecraft.");
    	}
    }

}