<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

require_once("TwitterAPIExchange.php");

class TwitterStalker {

    private static $config;
    private static $twitterStalker;
    private static $twitterURL;
    
    public static function init() {
        self::$config = json_decode(file_get_contents(MODPATH . DIRECTORY_SEPARATOR . "TwitterStalker" . DIRECTORY_SEPARATOR . "mod.json"));

        foreach (self::$config->twitters as &$twitter) {
            $twitter->lastCheck = time();
        }
        $settings = array(
            'oauth_access_token' => "40203287-O4c0eGXKTBxoyxCSieYXZ67JQaozd9osNfOYZ0SMs",
            'oauth_access_token_secret' => "bXnOxKgzbgckpJWu6uDx3eQrv9BCPFXbtMCBibeMOwU5W",
            'consumer_key' => "YuoxH94nHMRg0Ztq6KPPNI9xg",
            'consumer_secret' => "3jAbpr3bx1jfw9DOead0DSsMRRu6hGnUEYrBitmUCjHSBbyxs3"
        );

        self::$twitterStalker = new \TwitterAPIExchange($settings);
        self::$twitterURL = "https://api.twitter.com/1.1/statuses/user_timeline.json";
    }

    public static function hooks() {
        Hook::add("ping", array(__CLASS__, "TwitterChecker"));
    }

    public static function TwitterChecker($server) {
        foreach (self::$config->twitters as $twitterID => &$twitter) {
            $posts = json_decode(self::$twitterStalker->setGetfield("?screen_name={$twitterID}&count=20")->buildOauth(self::$twitterURL, "GET")->performRequest());

            $posts = array_reverse((array) $posts);
            
            foreach ($posts as $post) {
                if (strtotime($post->created_at) > $twitter->lastCheck) {
                    foreach ($twitter->channels as $channel) {
                        Bot::MSG($channel, "[TweetStalker] [{$post->user->screen_name}] {$post->text} posted at " . date("m/d/y H:m:s", strtotime($post->created_at)) . (!empty($twitter->append) ? $twitter->append : ""));
                    }
                }
            }
            
            $twitter->lastCheck = time();
        }
    }

}