<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;
use Tanzanite\Irc;

class Lastfm {

    private static $config;

	public static function init() {
        self::$config = json_decode(file_get_contents(MODPATH . DIRECTORY_SEPARATOR . "Lastfm" . DIRECTORY_SEPARATOR . "mod.json"));
	}

	public static function hooks() {
		Hook::add("privmsg", array(__CLASS__, "np"), array("command" => "/^!np$/"));
        Hook::add("privmsg", array(__CLASS__, "npAll"), array("command" => "/^!np->all$/"));
	}

	public static function np($nick, $ident, $host, $destination, $message, $command, $params) {
		
		if (!empty($params[1])) {
			$user = $params[1];
		} else {
		    foreach (self::$config->lastfm as $lastFM) {
		        if ($lastFM->nickname == $nick) {
		            $user = $lastFM->user;
                }
            }
		}

		$song = self::getCurrentSong($user);
		
		if ($song != "offline") {
			Bot::MSG($destination, "{$user} np: {$song}");
		} else {
		    Bot::MSG($destination, "{$user} is not playing music at the moment.");
        }
	}
        
    public static function npAll($nick, $ident, $host, $destination, $message, $command, $params) {

        foreach (self::$config->lastfm as $users) {
            $user = $users->user;
            $song = self::getCurrentSong($user);

            if ($song != "offline") {
                Bot::MSG($destination, "{$user} np: {$song}");
            }
        }
    }

	public static function getCurrentSong($user) {
		$url = "http://ws.audioscrobbler.com/2.0/?method=user.getRecentTracks&user={$user}&api_key=a1bbf4327a87ce0f8c9f7116c4131932&format=json";
		$response = json_decode(file_get_contents($url));

		foreach ($response->recenttracks->track as $track) {
			if ($track->{'@attr'}->nowplaying) {
				return "{$track->name} by {$track->artist->{'#text'}}";
			} else {
			    return "offline";
            }
		}
		return false;
	}
}