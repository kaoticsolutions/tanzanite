<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;
use Tanzanite\Module;

class ReloadModules
{

    private static $config;

    public static function init()
    {
        self::$config = json_decode(file_get_contents(MODPATH . DIRECTORY_SEPARATOR . "TwitterStalker" . DIRECTORY_SEPARATOR . "mod.json"));

    }

    public static function hooks()
    {
        Hook::add("privmsg", array(__CLASS__, "ReloadModules"), array("command" => "/^!reload$/"));
    }

    public static function ReloadModules($nick, $ident, $host, $destination, $message, $command, $params)
    {
        $module = new Module();
        if (empty($params[1])) {
            $reload = $module->reload();
            Bot::MSG($destination, "All modules have been successfully reloaded.");
        }
    }
}