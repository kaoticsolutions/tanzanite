<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class Steam {

    public static function init() {
        
    }

    public static function hooks() {
        Hook::add("privmsg", array(__CLASS__, "steam"), array("command" => "#!steam#"));
    }

    public static function steam($nick, $ident, $host, $destination, $message, $command, $params) {
        $string = implode(" ", $params);
        if (preg_match("/.*\+.*/", $string)) {
            $player = strstr($string, ' +', true);
        } else {
            $player = $string;
        }

        $steamID = self::findPlayer($player);

        if ($steamID == 0) {
            Bot::MSG($destination, "Steam account cannot be found.");
        } else {
            $Profile = self::Get_Player_Profile($steamID);
        }

        if ($Profile == 0) {
            Bot::MSG($destination, "Steam account cannot be found.");
        } elseif ($Profile == 1) {
            Bot::MSG($destination, "This person does not have a community profile.");
        } else {

            if ($Profile["Status"] == "online") {
                $String = $Profile["Name"] . " is currently " . $Profile["Status"] . ".";
            } elseif ($Profile["Status"] == "in-game") {
                $String = $Profile["Name"] . " is currently playing " . $Profile["Game"];
            } else {
                $String = $Profile["Name"] . " is currently " . $Profile["Status"] . ".";
            }
            
            Bot::MSG($destination, $String);


            if (preg_match("*\+games*", $message)) {
                $gameArray = self::Get_Player_Games($steamID);
                $gameString = "";
                foreach ($gameArray as $key => $gameName) {
                    $gameString .= ", $gameName";
                }
                $gameString = substr($gameString, 2);
                Bot::MSG($destination, $gameString);
            }
        }
    }

    private function findPlayer($player) {
        if (preg_match("/[0-9]{17}/", $player)) {
            return $player;
        } else {
            $steamID = self::Get_Player_ID64($player);
            print_r($steamID);

            if (strlen($steamID) == 1) {
                $steamID = self::lookupBySteamDisplayName($player);
                if (strlen($steamID) == 1) {
                    return false;
                } else {
                    $steamID = self::Get_Player_ID64($steamID);
                    return $steamID;
                }
            } else {
                return $steamID;
            }
        }
    }

    private function Get_Player_ID64($steamName) {
        $steamName = urlencode($steamName);
        $url = "http://steamcommunity.com/id/$steamName?xml=1";
        $xml = Bot::cURL($url, "xml");
        $steamID = $xml->steamID64;
        if (!$steamID) {
            $steamID = 0;
        }
        return $steamID;
    }

    private function Get_Player_Profile($steamID) {
        $url = "http://steamcommunity.com/profiles/$steamID?xml=1";
        $xml = Bot::cURL($url, "xml");

        $profile = array();

        if ($xml->steamID64) {
            $profile["Name"] = $xml->steamID;
            $profile["SteamID64"] = $xml->steamID64;
            $profile["Status"] = $xml->onlineState;
            $profile["StatusMessage"] = $xml->stateMessage;
            $profile["MemberSince"] = $xml->memberSince;
            $profile["Rating"] = $xml->steamRating;
            $profile["Hours"] = $xml->hoursPlayed2Wk;

            $profile["Game"] = $xml->inGameInfo->gameName;
            $profile["ServerIP"] = $xml->inGameServerIP;
        } else {
            $profile = 0;
        }

        if (preg_match("/This user has not yet set up their Steam Community profile/", $xml->privacyMessage)) {
            $profile = 1;
        }

        return $profile;
    }

    function lookupBySteamDisplayName($name) {
        $name = urlencode($name);
        $url = "http://steamcommunity.com/actions/Search?K=\"$name\"";

        $string = Bot::cURL($url, "string");

        if (preg_match("/\<a class\=\"linkTitle\" href\=\"http\:\/\/steamcommunity\.com\/id\/(.*)\"\>(.*)\<\/a\>/", $string, $matches)) {
            $steamID = $matches[1];
        } else {
            $steamID = 0;
        }


        return $steamID;
    }

    function Get_Player_Games($Steam_ID) {
        $url = "http://steamcommunity.com/profiles/$Steam_ID/games?xml=1&tab=all";
        $xml = Bot::cURL($url, "xml");

        $Games = array();

        foreach ($xml->games->game as $key => $game) {
            $games[] = "$game->name";
        }

        return $games;
    }

    private function Get_Player_Groups($Steam_ID) {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Player_Stats($Steam_ID) {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Group_List() {
        $url = "";

        $xml = Bot::cURL($url, "xml");
    }

    private function Get_Account_Stats($Steam_ID) {
        $html = file_get_contents("http://www.steamcalculator.com/id/$Steam_ID");
        if (preg_match_all('/\<div id\=\"rightdetail\"\>Found (.*?) Games with a value of \<h1\>(.*?) USD\<\/h1\>\<\/div\>/s', $html, $stats, PREG_SET_ORDER)) {

            $profileStats = array();

            foreach ($stats as $stat) {
                $profileStats["GameCount"] = $stat[1];
                $profileStats["Worth"] = $stat[2];
            }
            $profileStats["Status"] = 1;
        } else {
            $profileStats["Status"] = 0;
        }
        return $profileStats;
    }

}