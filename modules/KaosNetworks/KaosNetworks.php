<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class KaosNetworks {

	public static function init() {
	}

	public static function hooks() {
		Hook::add("FORUM_THREADADD", array( 
			__CLASS__, 
			"threadAdd"
		));
		
		Hook::add("FORUM_POSTADD", array( 
			__CLASS__, 
			"postAdd"
		));
		
		Hook::add("FORUM_USERADD", array( 
			__CLASS__, 
			"userAdd"
		));
	}

	public static function threadAdd($thread) {
		$thread = json_decode($thread);
		
		Bot::MSG("#kaos", "[New Thread] {$thread->subject} by {$thread->username} http://forums.kaosnetworks.com/thread-{$thread->tid}.html");
	}

	public static function postAdd($post) {
		$post = json_decode($post);
		
		Bot::MSG("#kaos", "[New Post] In thread {$post->subject} by {$post->username} http://forums.kaosnetworks.com/thread-{$post->tid}-post-{$post->pid}.html#pid{$post->pid}");
	}

	public static function userAdd($user) {
		$user = json_decode($user);
		
		Bot::MSG("#kaos", "[New User] {$user->username} http://forums.kaosnetworks.com/user-{$user->uid}.html");
	}

}