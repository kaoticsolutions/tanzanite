<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

class Quote {

	public static function init() {
	}

	public static function hooks() {
		Hook::add("privmsg", array( 
			__CLASS__, 
			"add"
		), array( 
			"command" => "/^!quote$/"
		));
		Hook::add("privmsg", array( 
			__CLASS__, 
			"getByID"
		), array( 
			"command" => "/^!quote->id$/"
		));
		Hook::add("privmsg", array( 
			__CLASS__, 
			"search"
		), array( 
			"command" => "/^!quote->search$/"
		));
		Hook::add("privmsg", array( 
			__CLASS__, 
			"getRandom"
		), array( 
			"command" => "/^!quote->rand$/"
		));
	}

	public static function add($nick, $ident, $host, $destination, $message, $command, $params) {
		if (! empty($params)) {
			$apiResponse = Bot::kaosAPI("quote", "index", "POST", array( 
				"quote" => implode(" ", $params), 
				"channel" => $destination
			));
			if (!empty($apiResponse)) {
				Bot::MSG($destination, "Quoted added with ID: {$apiResponse->id}.");
			} else {
				Bot::MSG($destination, "Something broke.");
			}
		} else {
			Bot::MSG($destination, "You must specify a quote... Dipshit.");
		}
	}

	public static function getByID($nick, $ident, $host, $destination, $message, $command, $params) {
		$apiResponse = Bot::kaosAPI("quote", "index/{$params[1]}", "GET");
		if (!empty($apiResponse)) {
			$quote = $apiResponse;
			Bot::MSG($destination, "[{$quote->id}] [{$quote->channel}] {$quote->quote} posted " . date("m/d/y H:i:s", strtotime($quote->created_at)));
		}
	}

	public static function search($nick, $ident, $host, $destination, $message, $command, $params) {
		$apiResponse = Bot::kaosAPI("quote", "index/" . urlencode(implode(" ", $params)), "GET");
		if (!empty($apiResponse)) {
			$quote = $apiResponse;
			Bot::MSG($destination, "[{$quote->id}] [{$quote->channel}] {$quote->quote} posted " . date("m/d/y H:i:s", strtotime($quote->created_at)));
		}
	}

	public static function getRandom($nick, $ident, $host, $destination, $message, $command, $params) {
		$apiResponse = Bot::kaosAPI("quote", "random", "GET");
		if (!empty($apiResponse)) {
			$quote = $apiResponse;
			Bot::MSG($destination, "[{$quote->id}] [{$quote->channel}] {$quote->quote} posted " . date("m/d/y H:i:s", strtotime($quote->created_at)));
		}
	}

}