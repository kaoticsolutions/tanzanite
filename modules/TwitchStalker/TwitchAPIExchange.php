<?php

class TwitchAPIExchange
{
    private $userAgent = 'KoasBot v2';
    public $connectTimeout = 30;
    public $timeout = 30;
    public $httpCode = 0;
    public $httpInfo = array();
    public $httpHeader = array();
    public $throwCurlErrors = true;
    private $apiVersion = 3;
    private $clientId;

    const URL_TWITCH = 'https://api.twitch.tv/kraken/';
    const URL_TWITCH_TEAM = 'http://api.twitch.tv/api/team/';
    const URI_AUTH = 'oauth2/authorize';
    const URI_AUTH_TOKEN = 'oauth2/token';
    const MIME_TYPE = 'application/vnd.twitchtv.v3+json';

    public function __construct(array $config = array())
    {
        $this->setClientId($config['client_id']);
    }

    public function setApiVersion($version)
    {
        if (ctype_digit(strval($version))) {
            $this->apiVersion = (int)$version;
        }
    }

    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    public function request($uri, $method = 'GET', $postfields = null)
    {
        return $this->generalRequest(self::URL_TWITCH . $uri, $method, $postfields);
    }

    public function teamRequest($uri, $method = 'GET', $postfields = null)
    {
        return $this->generalRequest(self::URL_TWITCH_TEAM . $uri . '.json', $method, $postfields);
    }

    private function generalRequest($uri, $method = 'GET', $postfields = null)
    {
        $this->httpInfo = array();

        $crl = $this->initCrl($uri, $method, $postfields);
        $response = curl_exec($crl);

        $this->httpCode = curl_getinfo($crl, CURLINFO_HTTP_CODE);
        $this->httpInfo = array_merge($this->httpInfo, curl_getinfo($crl));


        curl_close($crl);

        return json_decode($response);
    }

    private function initCrl($uri, $method, $postfields)
    {
        $optHttpHeader = array(
            'Expect:',
            'Accept: ' . sprintf(self::MIME_TYPE, $this->getApiVersion()),
            'Client-ID: ' . $this->getClientId(),
        );

        $crl = curl_init();
        curl_setopt($crl, CURLOPT_USERAGENT, $this->userAgent);
        curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $this->connectTimeout);
        curl_setopt($crl, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLOPT_HEADERFUNCTION, array($this, 'getHeader'));
        curl_setopt($crl, CURLOPT_HEADER, false);

        switch ($method) {
            case 'POST':
                curl_setopt($crl, CURLOPT_POST, true);
                break;
            case 'PUT':
                curl_setopt($crl, CURLOPT_CUSTOMREQUEST, 'PUT');
                break;
            case 'DELETE':
                curl_setopt($crl, CURLOPT_CUSTOMREQUEST, 'DELETE');
        }

        if ($postfields !== null) {
            curl_setopt($crl, CURLOPT_POSTFIELDS, ltrim($postfields, '?'));
            $optHttpHeader[] = 'Content-Length: ' . strlen($postfields);
        }

        curl_setopt($crl, CURLOPT_HTTPHEADER, $optHttpHeader);
        curl_setopt($crl, CURLOPT_URL, $uri);

        return $crl;
    }

    private function getHeader($ch, $header)
    {
        $i = strpos($header, ':');
        if (!empty($i)) {
            $key = str_replace('-', '_', strtolower(substr($header, 0, $i)));
            $value = trim(substr($header, $i + 2));
            $this->httpHeader[$key] = $value;
        }

        return strlen($header);
    }
}

class Stream
{
    protected $request;

    const URI_STREAM = 'streams/';
    const URI_STREAMS = 'streams';
    const URI_STREAMS_FEATURED = 'streams/featured';
    const URI_STREAM_SUMMARY = 'streams/summary';

    public function __construct(TwitchAPIExchange $request)
    {
        $this->request = $request;
    }

    public function checkStream($channel) {
        $streamStatus = $this->request->request(self::URI_STREAM . $channel);
        if ($streamStatus->stream != NULL) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getStream($channel)
    {
        return $this->request->request(self::URI_STREAM . $channel);
    }

    public function getStreams($queryString)
    {
        return $this->request->request(self::URI_STREAMS . $queryString);
    }
}