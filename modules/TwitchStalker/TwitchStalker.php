<?php

namespace Tanzanite\Modules;

use Tanzanite\Bot as Bot;
use Tanzanite\Hook as Hook;

require_once("TwitchAPIExchange.php");

class TwitchStalker
{

    private static $config;
    private static $twitchStalker;
    private static $twitchStalks;

    public static function init()
    {
        self::$config = json_decode(file_get_contents(MODPATH . DIRECTORY_SEPARATOR . "TwitchStalker" . DIRECTORY_SEPARATOR . "mod.json"));
        foreach (self::$config->twitches as &$twitch) {
            $twitch->lastCheck = time();
            $twitch->liveCheck = 0;
        }

        foreach (self::$config->config as $config) {
            $client_id = $config->client_id;
            $client_secret = $config->client_secret;
            $redirect_uri = $config->redirect_uri;
        }

        $settings = array(
            'client_id' => $client_id,
            'client_secret' => $client_secret,
            'redirect_uri' => $redirect_uri
        );
        self::$twitchStalker = new \TwitchAPIExchange($settings);
        self::$twitchStalks = new \Stream(self::$twitchStalker);
    }

    public static function hooks()
    {
        Hook::add("ping", array(__CLASS__, "TwitchAutoChecker"));
        Hook::add("privmsg", array(__CLASS__, "TwitchManualChecker"), array("command" => "/^!twitch$/"));
    }

    public static function TwitchAutoChecker($server)
    {
        foreach (self::$config->twitches as $twitchUser => &$twitch) {

            $checkStream = self::$twitchStalks->checkStream($twitchUser); // check if the user is streaming

            if ($checkStream !== 0) { // if the user is streaming, grab stream info

                $getStream = self::$twitchStalks->getStream($twitchUser); // communicate with twitch api
                $appendUser = $twitch->append;
                $twitch->lastCheck = time();


                $streamGame = $getStream->stream->game;
                $streamTitle = $getStream->stream->channel->status;
                $streamViewers = $getStream->stream->viewers;
                $streamURL = $getStream->stream->channel->url;
                $streamTWL = $getStream->stream->created_at;

                if (strtotime($streamTWL) > $twitch->lastCheck) {
                    foreach ($twitch->channels as $channel) {
                        Bot::MSG($channel, "[TwitchStalker] [{$twitchUser}] is streaming live! [Playing: {$streamGame}]:[{$streamTitle}] [Viewers: {$streamViewers}] [{$streamURL}]" . (!empty($appendUser) ? $appendUser : ""));
                    }
                }
            }
        }
    }

    public static function TwitchManualChecker($nick, $ident, $host, $destination, $message, $command, $params)
    {
        if (!empty($params[1])) {
            $twitchUser = $params[1]; // set twitch user from command
            $checkStream = self::$twitchStalks->checkStream($twitchUser);
            $getStream = self::$twitchStalks->getStream($twitchUser); // communicate with twitch api

            foreach (self::$config->twitches as $configUser => $twitch) {
                $appendUser = $twitch->append;
            }

            if ($checkStream == 0) {
                Bot::MSG($destination, "[TwitchStalker] {$twitchUser} is currently not streaming.");
            } else {
                $streamGame = $getStream->stream->game;
                $streamTitle = $getStream->stream->channel->status;
                $streamViewers = $getStream->stream->viewers;
                $streamURL = $getStream->stream->channel->url;

                Bot::MSG($destination, "[TwitchStalker] [{$twitchUser}] is streaming live! [Playing: {$streamGame}]:[{$streamTitle}] [Viewers: {$streamViewers}] [{$streamURL}]" . (!empty($appendUser) ? $appendUser : ""));
            }
        } else {
            Bot::MSG($destination, "[TwitchStalker] Error: Missing Twitch username. Syntax: !twitch username");
        }
    }
}
