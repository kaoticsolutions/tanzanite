function QueueManager() {

	this.config = {};
	this.client = {};

	this.init = function() {
		this.getConfig();
		this.initClient();
		this.looper();
	}

	this.getConfig = function() {
		var os = require("os");

		this.config = require(__dirname + '/../config/' + os.hostname() +  '.json');
	}

	this.initClient = function() {
		var dgram = require('dgram');
		this.client = dgram.createSocket("udp4");
	}

	this.sendUDP = function(message) {
		var message = Buffer(message);

		this.client.send(message, 0, message.length, this.config.udp.port, this.config.udp.ip, function(err, bytes) {
			client.close();
		});
	}

	this.looper = function() {
		qm = this;
		while(1) {
			setTimeout(qm.perform(), 5000);
		}
	}

	this.perform = function() {
		console.log("test");
		this.sendUDP("test");
	}
		
}

var QM = new QueueManager();
QM.init();