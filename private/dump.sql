SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
-- Table `kaosbot`.`quotes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaosbot`.`quotes` ;

CREATE  TABLE IF NOT EXISTS `kaosbot`.`quotes` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `channel` VARCHAR(255) NULL DEFAULT NULL ,
  `created` DATETIME NULL DEFAULT NULL ,
  `quote` TEXT NULL DEFAULT NULL ,
  `approved` TINYINT(3) UNSIGNED NULL DEFAULT '0' ,
  `created_by` INT(10) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `kaosbot`.`roles`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaosbot`.`roles` ;

CREATE  TABLE IF NOT EXISTS `kaosbot`.`roles` (
  `id` TINYINT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(50) NULL DEFAULT NULL ,
  `description` VARCHAR(254) NULL DEFAULT NULL ,
  `level` TINYINT(3) UNSIGNED NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = MyISAM
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `kaosbot`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaosbot`.`users` ;

CREATE  TABLE IF NOT EXISTS `kaosbot`.`users` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(25) NULL ,
  `password` VARCHAR(256) NULL DEFAULT NULL ,
  `password_salt` VARCHAR(32) NULL DEFAULT NULL ,
  `role_id` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1' ,
  `has_remote` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' ,
  `email` VARCHAR(254) NULL DEFAULT NULL ,
  `first_name` VARCHAR(50) NULL DEFAULT NULL ,
  `last_name` VARCHAR(50) NULL DEFAULT NULL ,
  `sms` VARCHAR(15) NULL DEFAULT NULL ,
  `created` DATETIME NULL DEFAULT NULL ,
  `created_by` INT(10) NULL ,
  `steam_id` VARCHAR(256) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) ,
  UNIQUE INDEX `username_UNIQUE` (`username` ASC) )
ENGINE = MyISAM
AUTO_INCREMENT = 32
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
