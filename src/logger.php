<?php

namespace Tanzanite;

class Logger {

    private static $method;

    public static function warning($message) {
        self::output(__FUNCTION__, $message);
    }

    public static function error($message) {
        self::output(__FUNCTION__, $message);
    }

    public static function info($message) {
        self::output(__FUNCTION__, $message);
    }

    public static function critical($message) {
        self::output(__FUNCTION__, $message);
        die();
    }

    private static function output($type, $message) {
        $string = "[" . date("m/d/y H:i:s") . "] [" . strtoupper($type) . "] {$message}\r\n";

        switch (self::$method) {
            default:              
            case "console":
                self::_writeToConsole($string);
                break;
        }
    }

    private static function _writeToConsole($string) {
        echo $string;
    }

}