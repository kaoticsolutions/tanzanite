<?php

namespace Tanzanite;

class Hook {

    private static $hooks;

    public static function init() {
        self::$hooks = array();
    }

    public static function add($hook, $call, $search = null) {
        $id = uniqid($hook);
        $hookCall = new \stdClass();
        $hookCall->call = $call;
        $hookCall->search = $search;

        self::$hooks[$hook][$id] = $hookCall;

        return $id;
    }

    public static function purgeModuleHooks() {
        foreach (self::$hooks as $hookName => $hook) {
            foreach ($hook as $id => $hookCall) {
                if (strstr($hookCall->call[0], "Tanzanite\\Modules\\")) {
                    unset(self::$hooks[$hookName][$id]);
                }
            }
        }
    }

    public static function delete($hook, $id) {
        unset(self::$hooks[$hook][$id]);
    }

    public static function trigger($hook, $args = array()) {
        if (!empty(self::$hooks[$hook])) {

            if (array_key_exists("nick", $args)) {
                if ($args["nick"] == Bot::$config->bot->nick) {
                    return false;
                }
            }

            foreach ($args as $key => $value) {
                if (is_int($key)) {
                    unset($args[$key]);
                } else {
                    $args[$key] = trim($args[$key]);
                }
            }

            if (in_array($hook, array("notice", "privmsg"))) {
                $string = $args["message"];
                $bits = explode(" ", $string);
                $args["command"] = $bits[0];
                unset($bits[0]);
                $args["params"] = $bits;
            }

            foreach (self::$hooks[$hook] as $id => $hookCall) {
                $hook = $hookCall->call;

                if (!empty($hookCall->search) && is_array($args)) {
                    $pass = true;
                    foreach ($hookCall->search as $param => $search) {
                        if (key_exists($param, $args)) {
                            if (!preg_match($search, $args[$param])) {
                                $pass = false;
                            }
                        }
                    }

                    if ($pass === true) {
                        call_user_func_array($hook, $args);
                    }
                } else {
                    call_user_func_array($hook, $args);
                }
            }
        }
    }

    public static function getAll() {
        return self::$hooks;
    }

}