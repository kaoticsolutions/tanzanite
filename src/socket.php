<?php

namespace Tanzanite;

use Tanzanite\Logger as Logger;

class Socket {

    public $socket;
    private $type;
    public $clients;

    public function __construct($type) {
        $this->type = $type;
        
        if ($type == "udp") {
            $this->clients = array();
        }
    }

    public function createServer($ip, $port) {
        if ($this->socket = stream_socket_server("udp://{$ip}:{$port}", $errno, $errstr, STREAM_SERVER_BIND)) {
            Logger::info("Listen server started on {$ip} using port {$port}.");
        } else {
            Logger::error("Could not start listening server.");
        }

        if (stream_set_blocking($this->socket, 0)) {
            Logger::info("Steam blocking disabled on listening server.");
        } else {
            Logger::warning("Cannot set stream blocking on server.");
        }
        
        return $this;
    }

    public function connect($server, $port, $ssl = false) {
        if ($ssl) {
            $context = stream_context_create();
            stream_context_set_option($context, 'ssl', 'verify_host', false);
            stream_context_set_option($context, 'ssl', 'allow_self_signed', true);

            $this->socket = stream_socket_client(($ssl == true ? "ssl://" : "") . $server . ":" . $port, $errno, $errstr, 60, STREAM_CLIENT_CONNECT, $context);
        } else {
            $this->socket = stream_socket_client(($ssl == true ? "ssl://" : "") . $server . ":" . $port, $errno, $errstr);
        }

        if (!empty($errstr)) {
            Logger::critical("Could not connect to IRC. ERROR: {$errstr}");
            return false;
        } else {
            Logger::info("Connected to {$server} on port {$port}.");
        }

        if (stream_set_blocking($this->socket, 0)) {
            Logger::info("Steam blocking disabled on IRC client.");
        } else {
            Logger::info("Cannot set stream blocking on IRC client.");
        }
        
        return $this;
    }

    public function read() {
        if ($this->type == "udp") {
            $buffer = stream_socket_recvfrom($this->socket, 2048);
        } else {
            $buffer = fgets($this->socket);
        }

        return $buffer;
    }

    public function write($string) {
        fwrite($this->socket, $string . "\r\n");
        fflush($this->socket);
    }

    public function kill() {
        socket_close($this->socket);
    }

}