<?php

namespace Tanzanite;

class Queue {

    public static function init() {
        
    }

    public static function add($executeAt, $function) {
        return \Job::add($executeAt, $function);
    }

    public static function delete($id) {
        
    }

    public function handle($id) {
        $job = \Job::getByID($id);

        if (!empty($job)) {
            $job = unserialize($job->function);

            try {
                $job();
            } catch (\Exception $e) {
                Logger::warn("Failled Callback");
            }
        }
    }

}