<?php

namespace Tanzanite;

use Tanzanite\Logger as Logger;
use Tanzanite\Hook as Hook;

class Module {

    public static $modules;
    private static $namespaceID;

    public static function init() {
        self::$modules = array();

        self::load();

    }

    private function load() {

        Hook::purgeModuleHooks();
        self::$namespaceID = uniqid("DYNMOD_");

        if (!empty(self::$modules)) {
            foreach (self::$modules as $key => $module) {
                Logger::info("Unloaded module {$module->name}");
                unset(self::$modules[$key]);
            }
        }

        $dirs = glob(MODPATH . DIRECTORY_SEPARATOR . "*");

        foreach ($dirs as $module) {
            $modName = pathinfo($module, PATHINFO_FILENAME);
            if (is_dir($module) && is_file($module . DIRECTORY_SEPARATOR . "mod.json") && is_file($module . DIRECTORY_SEPARATOR . $modName . ".php")) {
                $modContents = file_get_contents($module . DIRECTORY_SEPARATOR . $modName . ".php");
                $currentNamespace = preg_match("#(namespace (.*);)#", $modContents, $matches);
                file_put_contents($module . DIRECTORY_SEPARATOR . $modName . ".php", str_replace($matches[1], "namespace Tanzanite\\Modules\\" . self::$namespaceID . ";", $modContents));

                include($module . DIRECTORY_SEPARATOR . $modName . ".php");

                $modContents = file_get_contents($module . DIRECTORY_SEPARATOR . $modName . ".php");
                $currentNamespace = preg_match("#(namespace (.*);)#", $modContents, $matches);
                file_put_contents($module . DIRECTORY_SEPARATOR . $modName . ".php", str_replace($matches[1], "namespace Tanzanite\\Modules;", $modContents));

                $modObj = new \stdClass();
                $modObj->name = $modName;
                $modObj->path = $module;
                $modObj->config = json_decode(file_get_contents($module . DIRECTORY_SEPARATOR . "mod.json"));
                self::$modules[] = $modObj;

                if (method_exists("\\Tanzanite\\Modules\\" . self::$namespaceID . "\\{$modName}", "init")) {
                    call_user_func("\\Tanzanite\\Modules\\" . self::$namespaceID . "\\{$modName}::init");
                }

                if (method_exists("\\Tanzanite\\Modules\\" . self::$namespaceID . "\\{$modName}", "hooks")) {
                    call_user_func("\\Tanzanite\\Modules\\" . self::$namespaceID . "\\{$modName}::hooks");
                }

                Logger::info("Loaded module {$modName}");
            } else {
                Logger::warning("Couldn't load module {$modName}.");
            }
        }
    }

    public function reload() {
        self::load();
        Logger::info("All modules have been successfully reloaded.");
    }

}