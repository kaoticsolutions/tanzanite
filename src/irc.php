<?php

namespace Tanzanite;

use Tanzanite\Logger as Logger;

class Irc {

    private static $ircd;
    private static $regexes;

    public static function setType($type) {
        self::$ircd = $type;
        self::_loadParser();
    }

    private static function _loadParser() {
        if (file_exists(SRCPATH . DIRECTORY_SEPARATOR . "parsers" . DIRECTORY_SEPARATOR . self::$ircd . ".json")) {
            $regexes = json_decode(file_get_contents(SRCPATH . DIRECTORY_SEPARATOR . "parsers" . DIRECTORY_SEPARATOR . self::$ircd . ".json"));
            if (json_last_error() == JSON_ERROR_NONE) {
                self::$regexes = $regexes;
                Logger::info("Parser loaded.");
            } else {
                Logger::critical("Parser not valid.");
            }
        } else {
            Logger::critical("Parser file not found " . self::$ircd . ".json");
        }
    }

    public static function parse($string) {
        foreach (self::$regexes as $regex) {
            if (preg_match($regex->regex, $string, $matches)) {
                unset($matches[0]);
                $out = new \stdClass();
                $out->name = $regex->name;
                $out->args = $matches;
                return $out;
            }
        }

        return false;
    }

}