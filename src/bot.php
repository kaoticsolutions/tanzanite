<?php

namespace Tanzanite;

use Tanzanite\Logger as Logger;
use Tanzanite\Hook as Hook;
use Tanzanite\Module as Module;
use Tanzanite\Irc as Irc;
use Tanzanite\Socket as Socket;
use Tanzanite\Queue as Queue;

class Bot {

	public static $cache;

	private static $socketIRC;

	private static $socketUDP;

	public static $config;

	public static function init($env = null) {
		self::_loggerInit();
		
		self::_configInit($env);
		
		self::_queueInit();
		self::_hookSystemInit();
		self::_moduleSystemInit();
		
		self::_loadCoreHooks();
		self::_loadSockets();
		self::_udpInit();
		self::_ircInit();
		self::_listen();
	}

	private static function _configInit($env = null) {
		if (!empty($env)) {
			$hostname = $env;
		} else {
			$hostname = exec("hostname");
		}

		$configFile = BASEPATH . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "{$hostname}.json";

		if (file_exists($configFile)) {
			$config = json_decode(file_get_contents(BASEPATH . DIRECTORY_SEPARATOR . "config" . DIRECTORY_SEPARATOR . "{$hostname}.json"));
			if (json_last_error() == JSON_ERROR_NONE) {
				self::$config = $config;
				Logger::info("Config loaded: ".$configFile);
			} else {
				Logger::critical("Config not valid.");
			}
		} else {
			Logger::critical("Config not found.");
		}
	}

	private static function _queueInit() {
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "callback.php");
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "queue.php");
		Logger::info("Loaded Queue System");
	}

	private static function _hookSystemInit() {
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "hook.php");
		Logger::info("Loaded Hook System...");
		\Tanzanite\Hook::init();
	}

	private static function _moduleSystemInit() {
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "module.php");
		Logger::info("Loaded Module System...");
		\Tanzanite\Module::init();
		Hook::trigger("init");
	}

	private static function _loggerInit() {
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "logger.php");
		Logger::info("Logger initiated...");
	}

	public static function _loadSockets() {
		Logger::info("Attempting to load socket class...");
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "socket.php");
	}

	public static function _udpInit() {
		Hook::trigger("preloadUdpParser");
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "udp.php");
		Udp::init();
		Logger::info("Loaded UDP Parser...");
		Hook::trigger("postLoadUdpParser");
		
		Logger::info("Attempting to create UDP server...");
		
		$socket = new \Tanzanite\Socket("udp");
		self::$socketUDP = $socket->createServer(self::$config->udp->ip, self::$config->udp->port);
	}

	private static function _ircInit() {
		Hook::trigger("preloadIrcParser");
		require_once (SRCPATH . DIRECTORY_SEPARATOR . "irc.php");
		Logger::info("Loaded IRC Parser...");
		Hook::trigger("postLoadIrcParser");
		
		if (! empty(self::$config->server->type)) {
			Irc::setType(self::$config->server->type);
			Logger::info("IRCd type defined as " . self::$config->server->type . ".");
		} else {
			Logger::critical("IRCd type not defined in config.");
		}
		
		Hook::trigger("preIrcConnect");
		
		$socket = new Socket("tcp");
		self::$socketIRC = $socket->connect(self::$config->server->address, self::$config->server->port, self::$config->server->ssl);
		Hook::trigger("postIrcConnect");
	}

	public static function _loadCoreHooks() {
		Hook::add("postIrcConnect", array( 
			__CLASS__, 
			"ircConnect"
		));
		Hook::add("endOfMOTD", array( 
			__CLASS__, 
			"nsIdent"
		));
		Hook::add("endOfMOTD", array( 
			__CLASS__, 
			"joinChannels"
		));
		Hook::add("ping", array( 
			__CLASS__, 
			"pong"
		));
		Hook::add("nickInUse", array( 
			__CLASS__, 
			"tryNewNick"
		));

	}

	private static function _listen() {
		while (1) {
			$sockets = array();
			if (! empty(self::$socketUDP->socket)) {
				$sockets[] = self::$socketUDP->socket;
			}
			$sockets[] = self::$socketIRC->socket;

            $write  = NULL;
            $except = NULL;
			if (false === ($num_changed_streams = stream_select($sockets, $write, $except, 0))) {
				continue;
			} elseif ($num_changed_streams > 0) {
				foreach ($sockets as $socket) {
					if ($socket == self::$socketIRC->socket) {
						if ($buffer = self::$socketIRC->read()) {
							if ($parsed = Irc::parse($buffer)) {
								Hook::trigger($parsed->name, $parsed->args);
							}
							
							Logger::info($buffer);
						}
					} elseif ($socket == self::$socketUDP->socket) {
						if ($buffer = self::$socketUDP->read()) {
							if (preg_match("#HANDLE (.*)#", $buffer, $matches)) {
								Queue::handle($matches[1]);
							}
							
							if ($parsed = Udp::parse($buffer)) {
								Hook::trigger($parsed->name, $parsed->args);
							}
							
							Logger::info("UDP: {$buffer}");
						}
					}
				}
			}
			
			usleep(1);
		}
	}

	public static function kaosAPI($class, $function, $method, $args = array()) {
		$url = self::$config->kapi->url . "/{$class}/{$function}?apiKey=" . self::$config->kapi->key;
		
		$opts = array( 
			'http' => array( 
				'method' => $method
			)
		);
		
		if ($method == "POST") {
			$opts["http"]["content"] = json_encode((object) $args);
		}
		
		$context = stream_context_create($opts);
		$response = file_get_contents($url, false, $context);
		
		return json_decode($response);
	}

	public function ircConnect() {
		self::NICK(self::$config->bot->nick);
		self::RAW("USER " . self::$config->bot->ident . " * 8 :" . self::$config->bot->realname);
		
		if (! empty(self::$config->server->password))
			self::RAW("PASS " . self::$config->server->password);
	}

	public static function pong($server) {
		self::RAW("PONG :{$server}");
	}

	public static function joinChannels() {
		if (!empty(self::$config->bot->channels)) {
			foreach (self::$config->bot->channels as $channel) {
				self::JOIN($channel);
			}
		}
	}

	public static function nsIdent() {
		self::MSG("NickServ", "IDENTIFY " . self::$config->nickserv->password);
	}

	public function tryNewNick($server, $nickInUse) {
		$newNick = $nickInUse . rand(100, 99999);
		self::$config->bot->nick = $newNick;
		self::NICK($newNick);
	}

	/**
	 * Sends a PRIVMSG to a channel/nick on IRC.
	 * 
	 * @param string $destination
	 *        	Channel/User to send PRIVMSG to. (Required)
	 * @param string $message
	 *        	Message to send send. (Required)
	 */
	public static function MSG($destination, $message) {
		$message = str_split($message, 500);
		foreach ($message as $line) {
			self::$socketIRC->write("PRIVMSG $destination :$line");
		}
	}

	/**
	 * Sends a NOTICE to a channel/nick on IRC.
	 * 
	 * @param string $destination
	 *        	Channel/User to send PRIVMSG to. (Required)
	 * @param string $message
	 *        	Message to send send. (Required)
	 */
	public static function NOTICE($destination, $message) {
		$message = str_split($message, 500);
		foreach ($message as $line) {
			self::$socketIRC->write("NOTICE $destination :$line");
		}
	}

	/**
	 * Joins a channel with optional password.
	 * 
	 * @param string $channel
	 *        	Channel to join. (Required)
	 * @param string $password
	 *        	Password for +k channels. (Optional)
	 */
	public static function JOIN($channel, $password = null) {
		self::$socketIRC->write("JOIN $channel :$password");
	}

	/**
	 * Sends an action to a user/channel.
	 *
	 * @param string $channel
	 *        	Channel/User to send action to. (Required)
	 * @param string $message
	 *        	Action to perform. (Required)
	 */
	public static function ACTION($channel, $message) {
		self::$socketIRC->write("PRIVMSG $channel :ACTION $message");
	}

	/**
	 * Parts a channel with option message.
	 * 
	 * @param string $channel
	 *        	Channel to part. (Required)
	 * @param string $message
	 *        	Reason for parting. (Optional)
	 */
	public static function PART($channel, $message = "Parting...") {
		self::$socketIRC->write("PART $channel :$message");
	}

	/**
	 * Invites a user into channel.
	 * 
	 * @param string $channel
	 *        	Channel to invite a user to. (Required)
	 * @param string $nick
	 *        	User to invite. (Required)
	 */
	public static function INVITE($channel, $nick) {
		self::$socketIRC->write("INVITE $nick :$channel");
	}

	/**
	 * Kicks specified user with option message.
	 * 
	 * @param string $channel
	 *        	channel to kick user from. (Required)
	 * @param string $nick
	 *        	User to kick. (Required)
	 * @param string $message
	 *        	Reason for kicking. (Optional)
	 */
	public static function KICK($channel, $nick, $message = "Kicked") {
		self::$socketIRC->write("KICK $channel $Who :$message");
	}

	/**
	 * Sets mode on channel.
	 * 
	 * @param string $channel
	 *        	Channel to set mode(s) on. (Required)
	 * @param string $mode
	 *        	Mode to set (Required)
	 * @param string $params
	 *        	Optional params. (Optional)
	 */
	public static function MODE($channel, $mode, $params = "") {
		self::$socketIRC->write("MODE $channel $mode $params");
	}

	/**
	 * Sets mode on channel.
	 * (SUPERADMIN)
	 * 
	 * @param string $channel
	 *        	Channel to set mode(s) on. (Required)
	 * @param string $mode
	 *        	Mode to set (Required)
	 * @param string $params
	 *        	Optional params. (Optional)
	 */
	public static function SAMODE($channel, $mode, $params = "") {
		self::$socketIRC->write("SAMODE $channel $mode $params");
	}

	/**
	 * Changes the nick of the bot.
	 * 
	 * @param string $newNick
	 *        	New nickname. (Required)
	 */
	public static function NICK($newNick) {
		self::$socketIRC->write("NICK $newNick");
	}

	/**
	 * Makes the bot quit.
	 * 
	 * @param string $message
	 *        	Reason. (Optional)
	 */
	public static function QUIT($message = "Quiting...") {
		self::$socketIRC->write("QUIT $message");
	}

	/**
	 * Sets the topic of channel.
	 * 
	 * @param string $channel
	 *        	Channel to set topic on. (Required)
	 * @param string $topic
	 *        	Topic to set. (Required)
	 */
	public static function TOPIC($channel, $topic) {
		self::$socketIRC->write("TOPIC $channel :$topic");
	}

	/**
	 * Sends a raw string to IRC.
	 * 
	 * @param string $string
	 *        	Raw string. (Required)
	 */
	public static function RAW($string) {
		self::$socketIRC->write($string);
	}

	/**
	 * Bans and kicks user from channel.
	 * 
	 * @param string $channel
	 *        	Channel to kick user from. (Required)
	 * @param string $nick
	 *        	User to kick. (Required)
	 * @param string $message
	 *        	Reason for kicking. (Optional)
	 */
	public static function BAN($channel, $nick, $message) {
		self::$socketIRC->write("MODE $channel +b :$nick");
		self::KICK($channel, $nick, $message);
	}

	/**
	 * Sends a who information request.
	 * 
	 * @param string $target
	 *        	Channel name or user nickname to request data on. (Required)
	 */
	public static function WHO($target) {
		self::$socketIRC->write("WHO $target");
	}

	public function cURL($url, $returnMode = "xml") {
		$curlOptions = array( 
			CURLOPT_FORBID_REUSE => TRUE, 
			CURLOPT_FRESH_CONNECT => TRUE, 
			CURLOPT_CONNECTTIMEOUT => 2, 
			CURLOPT_DNS_CACHE_TIMEOUT => 2, 
			CURLOPT_TIMEOUT => 5, 
			CURLOPT_RETURNTRANSFER => TRUE
		);
		
		$curl = curl_init($url);
		curl_setopt_array($curl, $curlOptions);
		$curlData = curl_exec($curl);
		$curlInfo = curl_getinfo($curl);
		curl_close($curl);
		
		if ($curlInfo["http_code"] != 503) {
			if ($curlData) {
				if ($returnMode == "xml") {
					$returnData = new \SimpleXmlElement($curlData, LIBXML_NOCDATA);
				} elseif ($returnMode == "json") {
					$returnData = json_decode($curlData);
				} elseif ($returnMode == "string") {
					$returnData = $curlData;
				}
			}
		} else {
			return false;
		}
		
		return $returnData;
	}

}