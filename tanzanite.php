<?php 

namespace Tanzanite;

define('BASEPATH', __DIR__);
define('SRCPATH', BASEPATH . DIRECTORY_SEPARATOR . "src");
define('MODPATH', BASEPATH . DIRECTORY_SEPARATOR . "modules");
define('MODELPATH', BASEPATH . DIRECTORY_SEPARATOR . "models");
define('THIRDPARTYPATH', BASEPATH . DIRECTORY_SEPARATOR . "3rdparty");

require_once("src/bot.php");
\Tanzanite\Bot::init($argv[1]);



?>
